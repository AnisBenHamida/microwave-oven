﻿using MicrowaveOvenWithStatePattern;

namespace MicrowaveOven
{
    /// <summary>
    /// Timer for microwave oven heating that keeps track of whether it's running or not.
    /// </summary>
    public interface IHeatingTimerWithRunningState : IHeatingTimer
    {
        /// <summary>
        /// Gets a value indicating whether the timer is currently running.
        /// </summary>
        bool IsRunning { get; }

        /// <summary>
        /// Stops and resets the timer.
        /// </summary>
        void Stop();
    }
}

﻿using MicrowaveOvenInterfaces;
using System;

namespace MicrowaveOven
{
    public sealed class MicrowaveOvenController
    {
        private readonly IMicrowaveOvenHardware microwaveOvenHardware;
        private readonly IHeatingTimerWithRunningState heatingTimer;

        public MicrowaveOvenController(
            IMicrowaveOvenHardware microwaveOvenHardware,
            IHeatingTimerWithRunningState heatingTimer)
        {
            this.microwaveOvenHardware = microwaveOvenHardware ?? throw new ArgumentNullException();
            this.heatingTimer = heatingTimer ?? throw new ArgumentNullException();

            UpdateLight(isDoorOpen: microwaveOvenHardware.IsDoorOpen);

            microwaveOvenHardware.IsDoorOpenChanged += OnIsDoorOpenChanged;
            microwaveOvenHardware.StartButtonPressed += OnStartButtonPressed;
            heatingTimer.Finished += OnFinishedHeating;
        }

        public bool IsLightOn { get; private set; }

        private void UpdateLight(bool isDoorOpen)
        {
            IsLightOn = isDoorOpen;
        }

        private void OnIsDoorOpenChanged(bool isDoorOpen)
        {
            UpdateLight(isDoorOpen);

            if (isDoorOpen)
            {
                microwaveOvenHardware.TurnOffHeater();
                heatingTimer.Stop();
            }
        }

        private void OnStartButtonPressed(object sender, EventArgs e)
        {
            if (microwaveOvenHardware.IsDoorOpen)
            {
                return;
            }

            if (heatingTimer.IsRunning)
            {
                heatingTimer.AddOneMinute();
            }
            else
            {
                microwaveOvenHardware.TurnOnHeater();
                heatingTimer.StartOneMinuteTimer();
            }
        }

        private void OnFinishedHeating(object sender, EventArgs e)
        {
            microwaveOvenHardware.TurnOffHeater();
        }
    }
}

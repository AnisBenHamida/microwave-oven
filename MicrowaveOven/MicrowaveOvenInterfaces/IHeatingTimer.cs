﻿using System;

namespace MicrowaveOvenWithStatePattern
{
    /// <summary>
    /// Timer for microwave oven heating.
    /// </summary>
    public interface IHeatingTimer
    {
        /// <summary>
        /// Signals that the timer is finished.
        /// </summary>
        event EventHandler Finished;

        /// <summary>
        /// Starts a one minute long timer. Restarts it if it's already running.
        /// </summary>
        void StartOneMinuteTimer();

        /// <summary>
        /// Adds one minute to the currently running timer.
        /// </summary>
        void AddOneMinute();
    }
}

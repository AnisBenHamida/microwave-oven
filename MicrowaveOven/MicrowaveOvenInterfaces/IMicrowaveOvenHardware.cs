﻿using System;

namespace MicrowaveOvenInterfaces
{
    /// <summary>
    /// Interface to the microwave oven hardware.
    /// </summary>
    public interface IMicrowaveOvenHardware
    {
        /// <summary>
        /// Get a value indicating whether the door to the microwave oven is open or closed.
        /// </summary>
        bool IsDoorOpen { get; }

        /// <summary>
        /// Signals that the door has opened or closed.
        /// </summary>
        event Action<bool> IsDoorOpenChanged;

        /// <summary>
        /// Signals that the start button is pressed.
        /// </summary>
        event EventHandler StartButtonPressed;

        /// <summary>
        /// Turns on the microwave heater element.
        /// </summary>
        void TurnOnHeater();

        /// <summary>
        /// Turns off the microwave heater element.
        /// </summary>
        void TurnOffHeater();
    }
}

using Microsoft.VisualStudio.TestTools.UnitTesting;
using MicrowaveOven;
using MicrowaveOvenInterfaces;
using Moq;
using System;

namespace MicrowaveOvenTests
{
    /// <summary>
    /// Unit tests for <see cref="MicrowaveOvenController"/>.
    /// </summary>
    [TestClass]
    public class MicrowaveOvenControllerTests
    {
        private Mock<IMicrowaveOvenHardware> microwaveOvenHardware;
        private Mock<IHeatingTimerWithRunningState> heatingTimer;

        [TestMethod]
        public void OpeningDoorTurnsOnLight()
        {
            // ARRANGE
            var controller = CreateMicrowaveOvenController(isDoorInitiallyOpen: false);

            // ACT
            OpenDoor();

            // ASSERT
            Assert.IsTrue(controller.IsLightOn);
        }

        [TestMethod]
        public void ClosingDoorTurnsOffLight()
        {
            // ARRANGE
            var controller = CreateMicrowaveOvenController(isDoorInitiallyOpen: true);

            // ACT
            CloseDoor();

            // ASSERT
            Assert.IsFalse(controller.IsLightOn);
        }

        [TestMethod]
        public void OpeningDoorStopsHeater()
        {
            // ARRANGE
            var controller = CreateMicrowaveOvenController(isDoorInitiallyOpen: false);

            // ACT
            OpenDoor();

            // ASSERT
            microwaveOvenHardware.Verify(m => m.TurnOffHeater());
            heatingTimer.Verify(m => m.Stop());
        }

        [TestMethod]
        public void PressingStartWhenDoorIsOpenDoesNothing()
        {
            // ARRANGE
            var controller = CreateMicrowaveOvenController(isDoorInitiallyOpen: true);

            // ACT
            PressStartButton();

            // ASSERT
            heatingTimer.VerifyNoOtherCalls();
            microwaveOvenHardware.Verify(m => m.TurnOnHeater(), Times.Never);
        }

        [TestMethod]
        public void PressingStartWhenDoorClosedRunsHeaterForOneMinute()
        {
            // ARRANGE
            var controller = CreateMicrowaveOvenController(isDoorInitiallyOpen: false);

            // ACT
            PressStartButton();

            // ASSERT
            heatingTimer.Verify(m => m.StartOneMinuteTimer());
            microwaveOvenHardware.Verify(m => m.TurnOnHeater());
        }

        [TestMethod]
        public void PressingStartWhenHeaterAlreadyRunningAddsOneMinute()
        {
            // ARRANGE
            var controller = CreateMicrowaveOvenController(isDoorInitiallyOpen: false);
            heatingTimer.Setup(m => m.IsRunning).Returns(true);

            // ACT
            PressStartButton();

            // ASSERT
            heatingTimer.Verify(m => m.AddOneMinute());
        }

        [TestMethod]
        public void HeaterTurnedOffWhenTimerIsFinished()
        {
            // ARRANGE
            var controller = CreateMicrowaveOvenController(isDoorInitiallyOpen: false);

            // ACT
            RaiseHeatingTimerFinished();

            // ASSERT
            microwaveOvenHardware.Verify(m => m.TurnOffHeater());
        }

        private MicrowaveOvenController CreateMicrowaveOvenController(bool isDoorInitiallyOpen)
        {
            microwaveOvenHardware = new Mock<IMicrowaveOvenHardware>();
            heatingTimer = new Mock<IHeatingTimerWithRunningState>();

            microwaveOvenHardware.Setup(m => m.IsDoorOpen).Returns(isDoorInitiallyOpen);

            return new MicrowaveOvenController(
                microwaveOvenHardware.Object,
                heatingTimer.Object);
        }

        private void OpenDoor() => microwaveOvenHardware.Raise(m => m.IsDoorOpenChanged += null, true);

        private void CloseDoor() => microwaveOvenHardware.Raise(m => m.IsDoorOpenChanged += null, false);

        private void PressStartButton() => microwaveOvenHardware.Raise(m => m.StartButtonPressed += null, EventArgs.Empty);

        private void RaiseHeatingTimerFinished() => heatingTimer.Raise(m => m.Finished += null, EventArgs.Empty);
    }
}

﻿namespace MicrowaveOvenWithStatePattern
{
    public sealed class DoorClosedState : DoorState
    {
        private readonly MicrowaveOvenController microwaveOvenController;

        internal DoorClosedState(MicrowaveOvenController microwaveOvenController)
            : base(microwaveOvenController)
        {
            this.microwaveOvenController = microwaveOvenController;
        }

        public override LightState AllowedLightState => new LightOffState();

        protected override void OnDoorClosed()
        {
            // Door already closed
        }

        protected override void OnDoorOpened()
        {
            microwaveOvenController.DoorState = new DoorOpenState(microwaveOvenController);
            microwaveOvenController.HeatingState.TurnOffHeater();
        }
    }
}

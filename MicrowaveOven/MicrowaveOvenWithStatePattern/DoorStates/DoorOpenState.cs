﻿namespace MicrowaveOvenWithStatePattern
{
    public sealed class DoorOpenState : DoorState
    {
        private readonly MicrowaveOvenController microwaveOvenController;

        internal DoorOpenState(MicrowaveOvenController microwaveOvenController)
            : base(microwaveOvenController)
        {
            this.microwaveOvenController = microwaveOvenController;
        }

        public override LightState AllowedLightState => new LightOnState();

        protected override void OnDoorClosed()
        {
            microwaveOvenController.DoorState = new DoorClosedState(microwaveOvenController);
        }

        protected override void OnDoorOpened()
        {
            // Door already open
        }
    }
}

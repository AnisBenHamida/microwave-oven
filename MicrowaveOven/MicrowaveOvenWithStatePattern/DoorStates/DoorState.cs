﻿using System;

namespace MicrowaveOvenWithStatePattern
{
    public abstract class DoorState : IDisposable
    {
        private readonly MicrowaveOvenController microwaveOvenController;

        protected DoorState(MicrowaveOvenController microwaveOvenController)
        {
            this.microwaveOvenController = microwaveOvenController;
            microwaveOvenController.MicrowaveOvenHardware.IsDoorOpenChanged += OnIsDoorOpenChanged;
        }

        /// <summary>
        /// Gets the alllowed light state for this door state.
        /// </summary>
        public abstract LightState AllowedLightState { get; }

        public void Dispose()
        {
            microwaveOvenController.MicrowaveOvenHardware.IsDoorOpenChanged -= OnIsDoorOpenChanged;
            GC.SuppressFinalize(this);
        }

        protected abstract void OnDoorClosed();

        protected abstract void OnDoorOpened();

        private void OnIsDoorOpenChanged(bool isDoorOpened)
        {
            if (isDoorOpened)
            {
                OnDoorOpened();
            }
            else
            {
                OnDoorClosed();
            }
        }
    }
}

﻿namespace MicrowaveOvenWithStatePattern
{
    public sealed class HeaterOffState : HeaterState
    {
        internal HeaterOffState(MicrowaveOvenController microwaveOvenController)
            : base(microwaveOvenController)
        {
        }

        internal override void TurnOffHeater()
        {
            // Already turned off
        }

        protected override void StartButtonPressedWhenDoorClosed()
        {
            MicrowaveOvenController.MicrowaveOvenHardware.TurnOnHeater();
            MicrowaveOvenController.HeatingTimer.StartOneMinuteTimer();
            MicrowaveOvenController.HeatingState = new HeaterOnState(MicrowaveOvenController);
        }
    }
}
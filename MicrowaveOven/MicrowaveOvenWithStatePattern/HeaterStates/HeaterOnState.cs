﻿using System;

namespace MicrowaveOvenWithStatePattern
{
    public sealed class HeaterOnState : HeaterState
    {
        internal HeaterOnState(MicrowaveOvenController microwaveOvenController)
            : base(microwaveOvenController)
        {
            microwaveOvenController.HeatingTimer.Finished += OnHeatingTimerFinished;
        }

        internal override void TurnOffHeater()
        {
            MicrowaveOvenController.MicrowaveOvenHardware.TurnOffHeater();
            MicrowaveOvenController.HeatingState = new HeaterOffState(MicrowaveOvenController);
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            MicrowaveOvenController.HeatingTimer.Finished -= OnHeatingTimerFinished;
        }

        protected override void StartButtonPressedWhenDoorClosed()
        {
            // Heating already on, add one minute to heating timer
            MicrowaveOvenController.HeatingTimer.AddOneMinute();
        }

        private void OnHeatingTimerFinished(object sender, EventArgs e)
        {
            TurnOffHeater();
        }
    }
}

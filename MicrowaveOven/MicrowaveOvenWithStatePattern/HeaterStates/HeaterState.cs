﻿using System;

namespace MicrowaveOvenWithStatePattern
{
    public abstract class HeaterState : IDisposable
    {
        protected HeaterState(MicrowaveOvenController microwaveOvenController)
        {
            MicrowaveOvenController = microwaveOvenController;
            microwaveOvenController.MicrowaveOvenHardware.StartButtonPressed += OnStartButtonPressed;
        }

        protected MicrowaveOvenController MicrowaveOvenController { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                MicrowaveOvenController.MicrowaveOvenHardware.StartButtonPressed -= OnStartButtonPressed;
            }
        }

        protected abstract void StartButtonPressedWhenDoorClosed();

        internal abstract void TurnOffHeater();

        private void OnStartButtonPressed(object sender, EventArgs e)
        {
            if (MicrowaveOvenController.DoorState is DoorClosedState)
            {
                // Start button shouldn't do anything if door is open
                StartButtonPressedWhenDoorClosed();
            }
        }
    }
}

﻿using MicrowaveOvenInterfaces;
using System;

namespace MicrowaveOvenWithStatePattern
{
    public sealed class MicrowaveOvenController
    {
        private DoorState doorState;
        private HeaterState heatingState;

        /// <summary>
        /// Intitializes a new instance of <see cref="MicrowaveOvenController"/> with
        /// closed door and heater turned off.
        /// </summary>
        public MicrowaveOvenController(
            IMicrowaveOvenHardware microwaveOvenHardware,
            IHeatingTimer heatingTimer)
        {
            MicrowaveOvenHardware = microwaveOvenHardware ?? throw new ArgumentNullException();
            HeatingTimer = heatingTimer ?? throw new ArgumentNullException();

            DoorState = new DoorClosedState(this);
            HeatingState = new HeaterOffState(this);
        }

        internal IMicrowaveOvenHardware MicrowaveOvenHardware { get; }

        public DoorState DoorState
        {
            get => doorState;
            internal set
            {
                doorState?.Dispose();
                doorState = value;
                LightState = value.AllowedLightState;
            }
        }

        public LightState LightState { get; private set; }

        public HeaterState HeatingState
        {
            get => heatingState;
            internal set
            {
                heatingState?.Dispose();
                heatingState = value;
            }
        }

        internal IHeatingTimer HeatingTimer { get; }
    }
}

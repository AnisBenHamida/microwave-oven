﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MicrowaveOvenInterfaces;
using MicrowaveOvenWithStatePattern;
using Moq;
using System;

namespace MicrowaveOvenWithStatePatternTests
{
    /// <summary>
    /// Unit tests for <see cref="MicrowaveOvenController"/>.
    /// </summary>
    [TestClass]
    public class MicrowaveOvenControllerTests
    {
        private Mock<IMicrowaveOvenHardware> microwaveOvenHardware;
        private Mock<IHeatingTimer> heatingTimer;
        private MicrowaveOvenController controller;

        [TestInitialize]
        public void TestIntialize()
        {
            microwaveOvenHardware = new Mock<IMicrowaveOvenHardware>();
            heatingTimer = new Mock<IHeatingTimer>();

            controller = new MicrowaveOvenController(
                microwaveOvenHardware.Object,
                heatingTimer.Object);
        }

        [TestMethod]
        public void OpeningDoorTurnsOnLight()
        {
            // ACT
            OpenDoor();

            // ASSERT
            Assert.IsInstanceOfType(controller.LightState, typeof(LightOnState));
        }

        [TestMethod]
        public void ClosingDoorTurnsOffLight()
        {
            // ARRANGE
            OpenDoor();

            // ACT
            CloseDoor();

            // ASSERT
            Assert.IsInstanceOfType(controller.LightState, typeof(LightOffState));
        }

        [TestMethod]
        public void OpeningDoorStopsHeater()
        {
            // ARRANGE
            PressStartButton();

            // ACT
            OpenDoor();

            // ASSERT
            Assert.IsInstanceOfType(controller.HeatingState, typeof(HeaterOffState));
            microwaveOvenHardware.Verify(m => m.TurnOffHeater());
        }

        [TestMethod]
        public void PressingStartWhenDoorIsOpenDoesNothing()
        {
            // ARRANGE
            OpenDoor();

            // ACT
            PressStartButton();

            // ASSERT
            heatingTimer.VerifyNoOtherCalls();
            microwaveOvenHardware.Verify(m => m.TurnOnHeater(), Times.Never);
        }

        [TestMethod]
        public void PressingStartWhenDoorClosedRunsHeaterForOneMinute()
        {
            // ACT
            PressStartButton();

            // ASSERT
            heatingTimer.Verify(m => m.StartOneMinuteTimer());
            microwaveOvenHardware.Verify(m => m.TurnOnHeater());
        }

        [TestMethod]
        public void PressingStartWhenHeaterAlreadyRunningAddsOneMinute()
        {
            // ARRANGE
            PressStartButton();

            // ACT
            PressStartButton();

            // ASSERT
            heatingTimer.Verify(m => m.AddOneMinute());
        }

        [TestMethod]
        public void HeaterTurnedOffWhenTimerIsFinished()
        {
            // ARRANGE
            PressStartButton();

            // ACT
            RaiseHeatingTimerFinished();

            // ASSERT
            microwaveOvenHardware.Verify(m => m.TurnOffHeater());
        }

        private void OpenDoor() => microwaveOvenHardware.Raise(m => m.IsDoorOpenChanged += null, true);

        private void CloseDoor() => microwaveOvenHardware.Raise(m => m.IsDoorOpenChanged += null, false);

        private void PressStartButton() => microwaveOvenHardware.Raise(m => m.StartButtonPressed += null, EventArgs.Empty);

        private void RaiseHeatingTimerFinished() => heatingTimer.Raise(m => m.Finished += null, EventArgs.Empty);
    }
}
